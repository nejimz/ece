@extends('layouts.app')

@section('content')
    <div class="grid-container">
        <div class="item1"></div>
        <div class="item1">
            <div class="login-border">
                <h1 class="login-title">ECE</h1>
                <h4 class="login-title-sub">Electricity<br>Consumption<br>Estimator</h4>
                <form class="login-form" method="POST" action="{{ route('login_auth') }}">
                    {{ csrf_field() }}
                    <input class="input" id="email" type="email" name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <p class="login-warning-message">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                    <input class="input" id="password" type="password" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <p class="warning-message">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                    <br><br>
                    <button type="submit" class="button-success">Login</button>
                </form>
                <p>Don't have account? <a href="{{ route('register') }}">Register Now!</a></p>
            </div>
        </div>
        <div class="item1"></div>
    </div>
@endsection
