@extends('layouts.master')

@section('content')

    <br />
    @foreach($rows as $row)
    <div class="card">
    	<div class="card-content is-small">
    		<div class="content is-small">
    			<h1>{{ $row->title }}</h1>
    			{!! $row->description !!}
    			<br><br>
    			<h5>{{ $row->user->name }}<small><br />{{ $row->created_at->format('M. d, Y @ H:i') }}</small></h5>
    		</div>
    	</div>
    </div>
    @endforeach
        
@endsection
