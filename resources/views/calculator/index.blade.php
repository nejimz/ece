@extends('layouts.master')

@section('content')
<br>
<form id="calculator" action="#" method="post">
	<div class="card">
		<div class="card-content">
	        <h3 class="title is-3"><i class="fa fa-calculator"></i>&nbsp;General Appliance Calculator</h3>
			<div class="content">
				<div class="columns">
					<div class="column is-3">
						<div class="field">
							<div class="control has-icons-left">
								<input class="input is-large has-text-right" id="price" name="price" type="text" placeholder="Price" autocomplete="off" value="{{ $rate->rate or 0 }}" readonly="">
								<span class="icon is-medium is-left">
									<i class="fa fa-dollar fa-lg"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="column is-3">
						<div class="control has-icons-left">
							<div class="select is-large">
								<select id="appliance_id" name="appliance_id" autofocus="">
									<option value="">- Appliance -</option>
									@foreach($appliances as $appliance)
									<option value="{{ $appliance->wattage }}_-_{{ $appliance->name }}">{{ $appliance->name }}</option>
									@endforeach
								</select>
							</div>
							<span class="icon is-large is-left">
								<i class="fa fa-dropbox"></i>
							</span>
						</div>
					</div>
					<div class="column is-2">
						<div class="field">
							<div class="control has-icons-left">
								<input class="input is-large has-text-centered" id="wattage" name="wattage" type="text" placeholder="Wattage" autocomplete="off">
								<span class="icon is-medium is-left">
									<i class="fa fa-bolt fa-lg"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="column is-2">
						<div class="field">
							<div class="control has-icons-left">
								<input class="input is-large has-text-centered" id="hours" name="hours" type="text" placeholder="Hour/s" autocomplete="off">
								<span class="icon is-medium is-left">
									<i class="fa fa-hourglass fa-lg"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="column is-2">
						<div class="field">
							<div class="control has-icons-left">
								<input class="input is-large has-text-centered" id="days" name="days" type="text" placeholder="Day/s" autocomplete="off">
								<span class="icon is-medium is-left">
									<i class="fa fa-sun-o fa-lg"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="card-footer">
			<!--<a href="javascript::void(0)" class="card-footer-item"><i class="fa fa-plus fa-lg"></i>&nbsp;Add</a>-->
			<button class="card-footer-item button is-large" type="submit">
				<i class="fa fa-calculator fa-lg"></i>&nbsp;Calculate
			</button>
			<button class="card-footer-item button is-large" type="reset">
				<i class="fa fa-refresh fa-lg"></i>&nbsp;Reset
			</button>
			<!-- <a href="javascript::void(0)" class="card-footer-item"><i class="fa fa-refresh fa-lg"></i>&nbsp;Reset</a> -->
		</footer>
	</div>
</form>
<br>
<table id="appliances" class="table is-striped is-bordered is-fullwidth">
	<thead>
		<tr>
			<th width="5%"></th>
			<th width="55%">Appliance</th>
			<th width="10%" class="has-text-centered">Wattage</th>
			<th width="10%" class="has-text-centered">Hour/s</th>
			<th width="10%" class="has-text-centered">Day/s</th>
			<th width="10%" class="has-text-right">Cost</th>
		</tr>
	</thead>
	<tbody></tbody>
	<tfoot>
		<tr>
			<th colspan="3"></th>
			<th colspan="2">Monthly Bill</th>
			<th id="monthly-bill" class="has-text-right"></th>
		</tr>
	</tfoot>
</table>
@endsection

@section('scripts')
<script type="text/javascript">
$(function(){
	$('#appliance_id').change(function(){
		var appliance = $(this).val().split("_-_");
		$('#wattage').val(appliance[0]);
	});

	$('#calculator').submit(function(){
		var id = Math.floor(Math.random() * 10000);
		var icon = '<a href="javascript:void(0)" onclick="deleteRow(this)"><i class="fa fa-times fa-lg"></i></a>';
		var appliance = $('#appliance_id').val().split("_-_");
		var wattage = $('#wattage').val().trim();
		var hours = $('#hours').val().trim();
		var days = $('#days').val();
		var price = $('#price').val();

		if(price == '' || price == 0)
		{
			alert('Price is required!');
			return false;
		}
		else if(!isFloat(price) && isNaN(price))
		{
				alert('Price is must be a number!');
				return false;
		}
		if(appliance == '')
		{
			alert('Appliance is required!');
			return false;
		}
		if(isNaN(wattage) || wattage == '' || wattage == 0)
		{
			alert('Wattage is required!');
			return false;
		}
		if(isNaN(hours) || hours == '')
		{
			alert('Hour/s is required!');
			return false;
		}

		else if(hours < 1)
		{
			alert('Hour/s is minimum of 1!');
			return false;
		}
		else if(hours > 24)
		{
			alert('Hour/s is maximum of 24!');
			return false;
		}

		if(isNaN(days) || days == '' || days == 0)
		{
			alert('Day/s is required!');
			return false;
		}
		else if(days < 1)
		{
			alert('Day/s is minimum of 1!');
			return false;
		}
		else if(days > 30)
		{
			alert('Hour/s is maximum of 30!');
			return false;
		}

		var kwh = (wattage * hours * days) / 1000;
		var cost = (kwh * price).toFixed(2);
		var row = 	'<tr id="row' + appliance[0] + '' + id + '"><td class="has-text-centered">' + icon + '</td>' + 
					'<td>' + appliance[1] + '</td>' + 
					'<td class="has-text-centered">' + wattage + '</td>' + 
					'<td class="has-text-centered">' + hours + '</td>' + 
					'<td class="has-text-centered">' + days + '</td>' + 
					'<td class="has-text-right cost">' + cost + '</td></tr>';

		$('#appliances tbody').append(row);
		monthly_bill();
		return false;
	});
});
function isFloat(n)
{
	return Number(n) === n && n % 1 !== 0;
}
function deleteRow(r) {
	var i = r.parentNode.parentNode.rowIndex;
	document.getElementById("appliances").deleteRow(i);
	monthly_bill();
} 
function monthly_bill() {
	var monthly_bill = 0;
	$('#appliances tbody tr').each(function(key, value){
		var cost = parseFloat(value['lastElementChild']['textContent']);
		monthly_bill = cost + monthly_bill;
	});
	
	$('#monthly-bill').html(monthly_bill.toFixed(2));
} 
</script>
@endsection
