@if ($paginator->hasPages())
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        <ul class="pagination-list">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <!--li class="disabled"><span>&laquo;</span></li-->
                <li><a class="pagination-previous" disabled>Previous</a></li>
            @else
                {{-- <!--li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li--> --}}
                <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination-previous">Previous</a></li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    {{-- <!--li class="disabled"><span>{{ $element }}</span></li--> --}}
                    <li class="disabled"><a class="pagination-ellipsis" disabled>{{ $element }}</a></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            {{-- <!--li class="active"><span>{{ $page }}</span></li--> --}}
                            <li class="pagination-link is-current"><span>{{ $page }}</span></li>
                        @else
                            {{-- <!--li><a href="{{ $url }}">{{ $page }}</a></li--> --}}
                            <li><a class="pagination-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                {{-- <!--li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li--> --}}
                <li><a href="{{ $paginator->nextPageUrl() }}" rel="next" class="pagination-next">Next page</a></li>
            @else
                <!--li class="disabled"><span>&raquo;</span></li-->
                <li><a class="pagination-previous" disabled>Next page</a></li>
            @endif
        </ul>
    </nav>
@endif
