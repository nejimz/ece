@extends('layouts.master')

@section('content')
<br>
<div class="card">
	<div class="card-content">
        <h3 class="title is-3"><i class="fa fa-tachometer"></i>&nbsp;Monthly Usage Summary</h3>
		<div class="content">
			<div id="chart"></div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var days = [];
	var appliance_1 = [];
	var appliance_2 = [];

	for (var i = 0; i <= 29; i++) 
	{
		days[i] = i + 1;
		appliance_1[i] = Math.floor(Math.random() * 500);
		appliance_2[i] = Math.floor(Math.random() * 500);
	}

	$(function(){
		Highcharts.chart('chart', {
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: ''
		    },
            credits: {
            	enabled: false
            },
		    /*subtitle: {
		        text: 'Source: WorldClimate.com'
		    },*/
		    xAxis: {
		        categories: days//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		    },
		    yAxis: {
		        title: {
		            text: 'Wattage'
		        }
		    },
		    plotOptions: {
		        /*line: {
		            dataLabels: {
		                enabled: true
		            },
		            enableMouseTracking: false
		        }*/
		        series: {
		            label: {
		                connectorAllowed: false
		            },
		            pointStart: 1
		        }
		    },
		    series: [{
		        name: 'Appliance 1',
		        data: appliance_1//[7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
		    }/*, {
		        name: 'Appliance 2',
		        data: appliance_2//[3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
		    }*/]
		});
	});
</script>
@endsection
