@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('advisories.index') }}">Advisories</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
            </header>
            <div class="card-content">
                @include('layouts.validation-messages')
                <div class="content">
        
                    <form action="{{ $route }}" method="post">

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="title">Title</label>
                                </div>
                            </div>
                            <div class="column is-10">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="title" id="title" value="{{ $title }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="description">Description</label>
                                </div>
                            </div>
                            <div class="column is-10">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            <textarea rows="10" class="textarea" id="description" name="description">{{ $description }}</textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="columns field is-horizontal">
                            <div class="column is-2">
                                <div class="field-label is-normal"></div>
                            </div>
                            <div class="column is-2">
                                <div class="field-body is-fullwidth">
                                    <div class="field">
                                        <p class="control">
                                            {!! csrf_field() !!}
                                            {!! $method !!}
                                            <button class="button is-info is-normal">Submit</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="column is-8">&nbsp;</div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
<script type="text/javascript">
</script>
@endpush