<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ECE - Electricity Consumption Estimator</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar has-shadow is-dark" role="navigation" aria-label="main navigation">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="{{ route('home') }}" class="navbar-item">ECE</a>
                        <div class="navbar-burger burger" data-target="navMenu" onclick="toggleBurger()">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                    <?php
                    $segment = Request::segment(2);
                    $is_active = ' is-active';

                    $home = ($segment == '')? $is_active: '';
                    $calculator = ($segment == 'calculator')? $is_active: '';
                    $usage = ($segment == 'usage')? $is_active: '';
                    $appliances = ($segment == 'appliances')? $is_active: '';
                    $chart = ($segment == 'chart')? $is_active: '';
                    $settings = ($segment == 'settings')? $is_active: '';
                    ?>
                    <div class="navbar-menu" id="navMenu">
                        <div class="navbar-start">
                            <a class="navbar-item {{ $home }}" href="{{ route('home') }}"><i class="fa fa-home "></i>&nbsp;Home</a>
                            <a class="navbar-item {{ $calculator }}" href="{{ route('calculator.index') }}"><i class="fa fa-calculator "></i>&nbsp;Calculator</a>
                            <a class="navbar-item {{ $usage }}" href="{{ route('usage.index') }}"><i class="fa fa-tachometer "></i>&nbsp;Usage</a>
                            <a class="navbar-item {{ $appliances }}" href="{{ route('appliances.index') }}"><i class="fa fa-dropbox "></i>&nbsp;Appliances</a>

                            @if(Auth::user()->role == 0)
                            <!-- is-hidden-desktop is-hidden-touch-->
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link {{ $settings }}" href="javascript:void(0)" title="Users"><i class="fa fa-cogs"></i>&nbsp;Settings</a>
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="{{ route('users.index') }}" title="Create"><i class="fa fa-users"></i>&nbsp;User</a>
                                    <a class="navbar-item" href="{{ route('rate.index') }}" title="Create"><i class="fa fa-money"></i>&nbsp;Rates</a>
                                    <a class="navbar-item" href="{{ route('advisories.index') }}" title="Create"><i class="fa fa-file-o"></i>&nbsp;Advisories</a>
                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="navbar-end">
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link" href="#"><i class="fa fa-user"></i>&nbsp;{{ Auth::user()->name }}</a>

                                <div class="navbar-dropdown">
                                    <a href="{{ route('reset_password_form') }}" class="navbar-item" title="Reset Password">Reset Password</a>
                                    <a href="javascript::void(0)" class="navbar-item" onclick="document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>
            <div class="container">@yield('content')</div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        &copy; ECE, Inc {{ date('Y') }}
                    </p>
                </div>
            </div>
        </footer>
        <script src="{{ asset('js/highcharts.js') }}"></script>
        <script type="text/javascript">
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>
        @yield('scripts')
    </body>
</html>
