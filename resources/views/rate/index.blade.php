@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-dropbox"></i>&nbsp;Rates</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th width="5%" class="has-text-centered"><a href="{{ route('rate.create') }}"><i class="fa fa-plus-circle fa-lg"></i></a></th>
                <th width="20%">Date</th>
                <th width="55%">Rate</th>
                <th width="20%">Created At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td class="has-text-centered">
                    @if(Auth::user()->access == 0)
                    <a href="{{ route('rate.edit', $row->id) }}" title="Edit Rate Details">
                        <i class="fa fa-edit"></i>
                    </a>
                    @else
                    <i class="fa fa-edit"></i>
                    @endif
                </td>
                <td>{{ $row->date->toDateString() }}</td>
                <td>{{ number_format($row->rate, 2) }}</td>
                <td>{{ $row->created_at->toDateTimeString() }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
