@extends('layouts.master')

@section('content')
<br>
<div class="card">
	<div class="card-content">
        <h3 class="title is-3"><i class="fa fa-tachometer"></i>&nbsp;Monthly Usage</h3>
		<div class="content">
			<form id="monthly_usage" action="{{ route('usage.data') }}" method="get">
				<div class="columns">
					<div class="column is-7"></div>
					<div class="column is-2">
						<input type="date" id="start" name="start" class="input" />
					</div>
					<div class="column is-2">
						<input type="date" id="end" name="end" class="input" />
					</div>
					<div class="column is-1">
						<button class="button is-link" type="submit">Submit</button>
					</div>
				</div>
			</form>
			<br />
			<div id="chart"></div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var xhr_chart = null;
	var start = '{{ date("Y-m-01") }}';
	var end = '{{ date("Y-m-t") }}';
	chart(start, end);

	$(function(){
		$('#monthly_usage').submit(function(){
			var start = $('#start').val();
			var end = $('#end').val();
			chart(start, end);
			return false;
		});
	});

	function chart(start, end)
	{
		$('#chart').html();
		xhr_chart = $.ajax({
			type : 'get',
			url : "{{ route('usage.data') }}",
			data : 'start=' + start + '&end=' + end,
			cache : false,
			dataType : "json",
		beforeSend: function(xhr){
				if (xhr_chart != null)
				{
					xhr_chart.abort();
				}
			}
		}).done(function(data) {
			console.log(data['days']);
			console.log(data['appliances']);
			Highcharts.chart('chart', {
			    chart: {
			        type: 'spline'
			    },
			    title: {
			        text: ''
			    },
	            credits: {
	            	enabled: false
	            },
			    xAxis: {
			        categories: data['days']
			    },
			    yAxis: {
			        title: {
			            text: 'Wattage'
			        }
			    },
			    tooltip: {
			        crosshairs: true,
			        shared: true
			    },
			    plotOptions: {
			        series: {
			        	dataLabels: {
			        		enabled: true
			        	},
			            label: {
			                connectorAllowed: false
			            },
			            pointStart: 1
			        }
			    },
			    series: data['appliances']
			});

		}).fail(function(jqXHR, textStatus){
			//console.log('Request failed: ' + textStatus);
		});
	}

	/*$(function(){
			Highcharts.chart('chart', {
			    chart: {
			        type: 'line'
			    },
			    title: {
			        text: ''
			    },
	            credits: {
	            	enabled: false
	            },
			    subtitle: {
			        text: 'Source: WorldClimate.com'
			    },
			    xAxis: {
			        categories: 
			    },
			    yAxis: {
			        title: {
			            text: 'Wattage'
			        }
			    },
			    plotOptions: {
			        series: {
			            label: {
			                connectorAllowed: false
			            },
			            pointStart: 1
			        }
			    },
			    series: [
			    ]
			});
	});*/
</script>
@endsection
