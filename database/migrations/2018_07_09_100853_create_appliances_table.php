<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppliancesTable extends Migration
{
    public function up()
    {
        Schema::create('appliances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('wattage');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('appliances');
    }
}