<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyRatePerHoursTable extends Migration
{
    public function up()
    {
        Schema::create('daily_rate_per_hour', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->double('rate', 10, 2);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daily_rate_per_hour');
    }
}