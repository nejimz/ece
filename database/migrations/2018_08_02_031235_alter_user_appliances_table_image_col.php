<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserAppliancesTableImageCol extends Migration
{
    public function up()
    {
        if (Schema::hasTable('user_appliances'))
        {
            Schema::table('user_appliances', function (Blueprint $table) {
                $table->text('image')->after('days');
            });
        }
    }
}
