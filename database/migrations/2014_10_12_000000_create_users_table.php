<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->tinyInteger('role')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            'name' => 'John Doe',
            'email' => 'jdoe@gmail.com',
            'password' => bcrypt('p@ssw0rd')
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
