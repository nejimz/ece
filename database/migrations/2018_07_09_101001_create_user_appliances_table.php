<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAppliancesTable extends Migration
{
    public function up()
    {
        Schema::create('user_appliances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('appliance_id')->unsigned();
            $table->tinyInteger('hours');
            $table->tinyInteger('days');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('appliance_id')->references('id')->on('appliances');
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_appliances');
    }
}