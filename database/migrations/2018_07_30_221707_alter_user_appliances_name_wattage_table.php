<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserAppliancesNameWattageTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('user_appliances'))
        {
            Schema::table('user_appliances', function (Blueprint $table) {
                $table->string('wattage')->after('user_id');
                $table->string('name')->after('user_id');
                $table->dropColumn(['appliance_id']);
            });
        }
    }
}
