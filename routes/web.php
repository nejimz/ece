<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('/', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('login_auth');

$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register')->name('register_auth');

Route::prefix('portal')
->middleware('auth')
->group(function(){
	$this->post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('users/reset-password', 'UsersController@reset_password_form')->name('reset_password_form');
	Route::post('users/reset-password', 'UsersController@reset_password')->name('reset_password');

	Route::get('/', 'HomeController@index')->name('home');
	Route::resource('calculator', 'CalculatorController')->only(['index',]);

	Route::get('usage/data', 'UsageController@data')->name('usage.data');
	Route::resource('usage', 'UsageController');

	Route::resource('appliances', 'UserAppliancesController');

	Route::prefix('settings')
	->middleware('AdminAccess')
	->group(function(){
		Route::resource('set-appliances', 'AppliancesController')->only(['index', 'create', 'store', 'edit', 'update']);
		Route::resource('rate', 'RateController')->only(['index', 'create', 'store', 'edit', 'update']);
		Route::resource('advisories', 'AdvisoriesController')->only(['index', 'create', 'store', 'edit', 'update']);

		Route::get('users/generate-password', 'UsersController@generate_password')->name('generate_password');
		Route::resource('users', 'UsersController');
	});
});
