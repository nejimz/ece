let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
		'resources/assets/js/app.js'
	], 'public/js');

mix.scripts([
	'node_modules/highcharts/modules/exporting.js',
],  'public/js/exporting.js');

mix.scripts([
	'node_modules/highcharts/highcharts.js'
],  'public/js/highcharts.js');


mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
	'node_modules/jquery-ui-bundle/jquery-ui.min.css',
	'node_modules/jquery-ui-bundle/jquery-ui.structure.min.css',
	'node_modules/jquery-ui-bundle/jquery-ui.theme.min.css'
	], 'public/css/jquery.css');

mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts/font-awesome');