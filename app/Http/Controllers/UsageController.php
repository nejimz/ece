<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DailyRatePerHour;
use App\UserAppliance;
use Auth;
use \Carbon\Carbon;

class UsageController extends Controller
{
	public function index()
	{
		#$user = Auth::user();
		#$appliances = UserAppliance::whereUserId($user->id)->orderBy('name', 'ASC')->get();
		#$arr = $this->data($user->id);
		#$appliances = json($arr);
		#dd($appliances);
		#return response()->json($appliances);

		#$data = compact('appliances');
        return view('usage.index');
	}

	public function data(Request $request)
	{
		$user = Auth::user();
		$start = Carbon::parse($request->start);
		$end = Carbon::parse($request->end);
		$appliances = UserAppliance::whereUserId($user->id)->orderBy('name', 'ASC')->get();
		#$rate = $this->latest_rate();
		$dates = [];
		for ($start=$start; $start->lte($end); $start->addDay())
		{ 
			$dates[] = $start->format('Y-m-d');
		}

		$data = [];
		$data['days'] = [];
		$n = 0;
		foreach($appliances as $appliance)
		{
			$rate = 0;
			$last_rate = 0;
			$total_cost = 0;
			$wattage = intval($appliance->wattage);
			$hours = intval($appliance->hours);
			$kwh = ($wattage * $hours * 1) / 1000;
			$data['appliances'][$n]['name'] = $appliance->name;

			foreach ($dates as $date)
			{
				if(!in_array($date, $data['days']))
				{
					$data['days'][] = date('m-d', strtotime($date));
				}
				#$rate = $this->latest_rate($date);
				$row = DailyRatePerHour::where('date', $date)->orderBy('date', 'DESC')->first();
				if($row)
				{
					$rate = $row->rate;
					$last_rate = $rate;
				}
				#dd($rate);
				$cost = $kwh * $rate;
				$data['appliances'][$n]['data'][] = floatval(number_format($cost, 2));
			}
			$n++;
		}

		#dd($data);

		return response()->json($data);
	}

	public function latest_rate($date)
	{
		$row = DailyRatePerHour::where('date', $date)->orderBy('date', 'DESC')->first();
		if($row)
		{
			return $row->rate;
		}

		return 0;
	}

	public function data_backup()
	{
		$number_of_days = 1;
		$user = Auth::user();
		$n = 0;
		$data = [];
		$appliances = UserAppliance::whereUserId($user->id)->orderBy('name', 'ASC')->get();
		$rate = $this->latest_rate();
		#dd($appliances);
		foreach ($appliances as $row)
		{
			$total_cost = 0;
			$wattage = intval($row->wattage);
			$hours = intval($row->hours);
			$kwh = ($wattage * $hours * $number_of_days) / 1000;
			$cost = $kwh * $rate;
			$data['appliances'][$n]['name'] = $row->name;

			for ($day = 0; $day <= 30; $day++) 
			{
				$data['appliances'][$n]['data'][$day] = floatval(number_format($cost, 2));
				$total_cost += $cost;
			}

			$n++;
		}

		for ($day = 0; $day <= 30; $day++) 
		{
			$data['days'][$day] = $day + 1;
		}

		return response()->json($data);
	}
}
