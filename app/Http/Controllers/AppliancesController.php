<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Appliance;

class AppliancesController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$query = new Appliance;

		if($request->has('search'))
		{
			$search = trim($request->search);
			$query = $query->where('name', 'LIKE', "$search%");
		}

		$rows = $query->orderBy('name', 'ASC')->paginate(10);

		$data = compact('search', 'rows');
		return view('appliances.index', $data);
	}

	public function create()
	{
		$route = route('set-appliances.store');
		$method = '';
		$name = old('name');
		$wattage = old('wattage');

		$data = compact('route', 'method', 'name', 'wattage');
		return view('appliances.create-form', $data);
	}

	public function store(Requests\AppliancesStoreRequest $request)
	{
		$input = $request->only('name', 'wattage');
		Appliance::create($input);
        return redirect()->route('set-appliances.create')->with('success', 'Appliance successfully added!');
	}

	public function edit($id)
	{
		$route = route('set-appliances.update', $id);
		$method = method_field('PUT');
		$row = Appliance::first();
		$name = $row->name;
		$wattage = $row->wattage;

		$data = compact('route', 'method', 'name', 'wattage');
		return view('appliances.create-form', $data);
	}

	public function update($id, Requests\AppliancesUpdateRequest $request)
	{
		$input = $request->only('name', 'wattage');
		Appliance::whereId($id)->update($input);
        return redirect()->route('set-appliances.edit', $id)->with('success', 'Appliance successfully updated!');
	}
}
