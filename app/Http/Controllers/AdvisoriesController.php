<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Advisory;
use Auth;

class AdvisoriesController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$query = new Advisory;

		if($request->has('search'))
		{
			$search = trim($request->search);
			$query = $query->where('title', 'LIKE', "$search%");
		}

		$rows = $query->orderBy('title', 'ASC')->paginate(10);

		$data = compact('search', 'rows');
		return view('advisory.index', $data);
	}

	public function create()
	{
		$route = route('advisories.store');
		$method = '';
		$title = old('title');
		$description = old('description');

		$data = compact('route', 'method', 'title', 'description');
		return view('advisory.create-form', $data);
	}

	public function store(Requests\AdvisoriesStoreRequest $request)
	{
		$input = $request->only('title', 'description');
		$input['user_id'] = Auth::user()->id;
		Advisory::create($input);
        return redirect()->route('advisories.create')->with('success', 'Advisory successfully added!');
	}

	public function edit($id)
	{
		$route = route('advisories.update', $id);
		$method = method_field('PUT');
		$row = Advisory::first();
		$title = $row->title;
		$description = $row->description;

		$data = compact('route', 'method', 'title', 'description');
		return view('advisory.create-form', $data);
	}

	public function update($id, Requests\AdvisoriesUpdateRequest $request)
	{
		$input = $request->only('title', 'description');
		Advisory::whereId($id)->update($input);
        return redirect()->route('advisories.edit', $id)->with('success', 'Advisory successfully updated!');
	}
}
