<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Appliance;
use App\UserAppliance;
use Auth;

class UserAppliancesController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$user = Auth::user();
		$query = UserAppliance::whereUserId($user->id);

		if($request->has('search'))
		{
			$search = trim($request->search);
			$query = $query->where('name', 'LIKE', "$search%");
		}

		$rows = $query->orderBy('name', 'ASC')->paginate(10);

		$data = compact('search', 'rows');
		return view('user.appliances.index', $data);
	}

	public function create()
	{
		$method = '';
		$route = route('appliances.store');
		$rows = Appliance::get();
		$name = old('name');
		$wattage = old('wattage');
		$hours = old('hours');
		$days = old('days');

		$data = compact('route', 'method', 'rows', 'name', 'wattage', 'hours', 'days');
		return view('user.appliances.create-form', $data);
	}

	public function store(Requests\UserAppliancesStoreRequest $request)
	{
		$user = Auth::user();
		$input = $request->only('name', 'wattage', 'days', 'hours');
		$input['user_id'] = $user->id;
		UserAppliance::create($input);
        return redirect()->route('appliances.create')->with('success', 'Appliance successfully added!');
	}

	public function edit($id)
	{
		$route = route('appliances.update', $id);
		$method = method_field('PUT');
		$rows = Appliance::get();
		$row = UserAppliance::whereId($id)->first();
		$name = $row->name;
		$wattage = $row->wattage;
		$hours = $row->hours;
		$days = $row->days;

		$data = compact('route', 'method', 'rows', 'name', 'wattage', 'hours', 'days');
		return view('user.appliances.create-form', $data);
	}

	public function update($id, Requests\UserAppliancesUpdateRequest $request)
	{
		$user = Auth::user();
		$input = $request->only('name', 'wattage', 'days', 'hours');
		$input['user_id'] = $user->id;
		UserAppliance::whereId($id)->update($input);
        return redirect()->route('appliances.edit', $id)->with('success', 'Appliance successfully updated!');
	}
}
