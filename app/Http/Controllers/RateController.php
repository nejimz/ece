<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DailyRatePerHour;

class RateController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$query = new DailyRatePerHour;

		if($request->has('search'))
		{
			$search = trim($request->search);
			$query = $query->where('name', 'LIKE', "$search%");
		}

		$rows = $query->orderBy('date', 'DESC')->paginate(10);

		$data = compact('search', 'rows');
		return view('rate.index', $data);
	}

	public function create()
	{
		$route = route('rate.store');
		$method = '';
		$date = old('date');
		$rate = old('rate');

		$data = compact('route', 'method', 'date', 'rate');
		return view('rate.create-form', $data);
	}

	public function store(Requests\RateStoreRequest $request)
	{
		$input = $request->only('date', 'rate');
		DailyRatePerHour::create($input);
        return redirect()->route('rate.create')->with('success', 'Rate successfully added!');
	}

	public function edit($id)
	{
		$route = route('rate.update', $id);
		$method = method_field('PUT');
		$row = DailyRatePerHour::first();
		$date = $row->date->toDateString();
		$rate = $row->rate;

		$data = compact('route', 'method', 'date', 'rate');
		return view('rate.create-form', $data);
	}

	public function update($id, Requests\RateUpdateRequest $request)
	{
		$input = $request->only('date', 'rate');
		DailyRatePerHour::whereId($id)->update($input);
        return redirect()->route('rate.edit', $id)->with('success', 'Rate successfully updated!');
	}
}
