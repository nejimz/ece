<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAppliance;
use App\DailyRatePerHour;
use Auth;

class CalculatorController extends Controller
{
	public function index()
	{
		$user = Auth::user();
		$appliances = UserAppliance::whereUserId($user->id)->orderBy('name', 'ASC')->get();
		$rate = DailyRatePerHour::orderBy('date', 'DESC')->first();
		$data = compact('appliances', 'rate');
        return view('calculator.index', $data);
	}
}
