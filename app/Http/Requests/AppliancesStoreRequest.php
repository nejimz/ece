<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppliancesStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:appliances,name', 
            'wattage' => 'required|numeric'
        ];
    }

     public function attributes()
    {
        return [
            'name' => 'Name', 
            'wattage' => 'Wattage'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'numeric'    => ':attribute is not a number.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
