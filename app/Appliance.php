<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appliance extends Model
{
    public $table = 'appliances';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'name', 'wattage', 'created_at', 'updated_at' 
	];

	public function user()
	{
    	return $this->hasOne('App\UserAppliance', 'appliance_id', 'id');
	}
}
