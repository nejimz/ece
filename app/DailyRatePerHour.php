<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyRatePerHour extends Model
{
    public $table = 'daily_rate_per_hour';
    public $timestamps = true;

    protected $dates = [
        'date', 'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'date', 'rate', 'created_at', 'updated_at' 
	];
}
