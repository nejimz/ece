<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    public $table = 'usage';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'user_id', 'appliance_id', 'created_at', 'updated_at' 
	];

	public function appliance()
	{
    	return $this->belongsTo('App\Appliance', 'appliance_id', 'id');
	}
}
