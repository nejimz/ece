<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAppliance extends Model
{
    public $table = 'user_appliances';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'name', 'wattage', 'hours', 'days', 'image', 'created_at', 'updated_at' 
	];
}
