<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisory extends Model
{
    public $table = 'advisories';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'title', 'description', 'user_id', 'created_at', 'updated_at' 
	];

	public function user()
	{
    	return $this->belongsTo('App\User', 'user_id', 'id');
	}
}
